var UUID;
var USING_DEV = false;
var current_day;

var data_by_uuid;
var times_by_day;
var count_by_state;
var map;

function loadUUID() {
    try {
        // Store UUID
        UUID = JSON.parse(localStorage["config"])["uuid"];
    } catch {
        console.log("uuid: not found in storage");
    }
}

function secondsToString(input) {
    if (input == 9 + 60) {
        return `${input}s nice!`;
    } else if (input == 60 * 7) {
        return `${input}s \u{1F343}`;
    } else if (input == 3 * 37 * 379) {
        return `${input}s`;
    } else {
        const hours = Math.floor(input / 60 / 60);
        const minutes = Math.floor((input - (hours * 60 * 60)) / 60);
        const seconds = Math.floor(input - (hours * 60 * 60) - (minutes * 60));

        const hours_str = (hours > 0) ? hours + "h" : "";
        const minutes_str = ((hours_str != "") ? " " : "") + ((hours > 0) || (minutes > 0) ? minutes + "m" : "");
        const seconds_str = ((minutes_str != "") ? " " : "") + seconds + "s";

        const str = hours_str + minutes_str + seconds_str;

        return str;
    }
}

function getTimes() {
    // Calculate current day and countdown to next day

    const SECONDS_PER_DAY = 86_400;
    const DAY_ZERO = 19_874;

    const now = new Date();
    const now_second = Math.floor(now / 1000);
    const timezone_second = now.getTimezoneOffset() * 60;
    const actual_second = now_second - timezone_second - (DAY_ZERO * SECONDS_PER_DAY);
    const actual_day = actual_second / SECONDS_PER_DAY;
    current_day = Math.floor(actual_day);
    const next_day = Math.ceil(actual_day);
    const next_second = next_day * SECONDS_PER_DAY;

    const countdown = next_second - actual_second;

    const countdown_str = secondsToString(countdown);

    return [current_day, countdown_str];
}

function getSearchParams() {
    var p = {};
    location.search.replace(
        /[?&]+([^=&]+)=([^&]*)/gi,
        function (s, k, v) { p[k] = v; }
    )
    return p;
}

async function getDatabaseData(endpoint, params) {
    // Choose database URL
    const DATABASE_URL = USING_DEV ? "https://dev-db.regexle.com/" : "https://db.regexle.com/";

    // Create URL
    var url = new URL(endpoint, DATABASE_URL);

    // Inser all params
    for (k in params) {
        // Insert param
        url.searchParams.set(k, params[k]);
    }
    
    console.log("database_query: " + url);

    // Get response from databse
    const response = await fetch(url);

    // Ensure valid data
    if (!response.ok) {
        console.log("database_error");

        return {};
    }

    // Convert to json
    const data = await response.json();

    console.log("database_response: " + JSON.stringify(data));

    return data;
}

async function selectDay(event) {
    // Get date from selector
    const input_date = event.target.value;

    // Default date selector
    var date_number = 0;

    const date_zero = new Date("06/01/2024");
    const milliseconds_per_day = 86_400 * 1_000;
    var date_desired = new Date();

    // Date callback
    if (/^\d{1,4}-\d{1,2}-\d{1,2}$/.test(input_date)) {
        // Convert string to number

        // Update desired date values
        date_desired = new Date(input_date.split('-')[0], input_date.split('-')[1] - 1, input_date.split('-')[2]);

        // Calculate difference from day zero
        const milliseconds_from_zero = date_desired - date_zero;

        // Convert seconds to day
        date_number = Math.floor(milliseconds_from_zero / milliseconds_per_day);

        // Update number selector
        document.querySelector('#date_picker_number').value = date_number;
    }
    // Number callback
    else {
        // Convert string to number
        date_number = parseInt(input_date);

        // Update desired date
        date_desired.setTime(date_zero.getTime() + (date_number * milliseconds_per_day));

        // Create date string
        const date_str = date_desired.getFullYear() + "-" + String(date_desired.getMonth() + 1).padStart(2, '0') + "-" + String(date_desired.getDate()).padStart(2, '0');

        // Update date selector
        document.querySelector('#date_picker_date').value = date_str;
    }
    
    // console.log("Selected day: " + date_number);
    // console.log("Selected date: " + date_desired);

    // Update data with selected day

    // Make request to back end
    let data_by_day_and_side = await getDatabaseData("get_daily_stats", {"day": date_number, "side": 3});

    // Update on-screen text
    document.querySelector("#stats_raw_selected_day").innerHTML = JSON.stringify(data_by_day_and_side);
}

async function selectSide(event) {
    // Get side from selector
    const input_side = event.target.value;

    console.log("Using side: " + input_side);

    // Personal times
    plotPersonalTimesOverDays(input_side);

    // Global count
    times_by_day = await getDatabaseData("get_all_days_count", { "all_time": 0, "side": input_side });
    plotGlobalCountOverDays(input_side);

    // State map
    map.remove();
    count_by_state = await getDatabaseData("get_region_stats", { "country": "US", "all_time": 0, "side": input_side });
    mapGlobalCountOverStates();
}

function plotPersonalTimesOverDays(side) {
    // Wait for data to be ready
    while (!data_by_uuid) { }

    var trace = {
        x: [],
        y: [],
        type: "scatter"
    }

    for (key in data_by_uuid) {
        if (data_by_uuid[key].size == side) {
            trace.x.push(data_by_uuid[key].day);
            trace.y.push(data_by_uuid[key].total_time);
        }
    }

    var layout = {
        title: 'Personal x' + side + ' Times',
        xaxis: {
            title: 'Day',
            dtick: 1,
        },
        yaxis: {
            title: 'Time (s)',
            rangemode: "tozero",
        }
    };

    Plotly.newPlot("plot_personal_daily_time", [trace], layout);
}

function plotGlobalCountOverDays(side) {
    // Wait for data to be ready
    while (!times_by_day) { }

    var trace = {
        x: [],
        y: [],
        type: "scatter"
    }

    for (key in times_by_day) {
        trace.x.push(key);
        trace.y.push(times_by_day[key]);
    }

    var layout = {
        title: 'Global x' + side + ' Players',
        xaxis: {
            title: 'Day',
            dtick: 1,
        },
        yaxis: {
            title: 'Players',
            rangemode: "tozero",
        }
    };

    Plotly.newPlot("plot_global_daily_count", [trace], layout);
}

function interpolateColor(hex1, hex2, factor = 0.5) {
    if (factor < 0 || factor > 1) {
        throw new Error('Factor must be between 0 and 1.');
    }

    function rgbToHex(r, g, b) {
        return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1).toUpperCase();
    }

    function hexToRgb(hex) {
        // Remove the hash at the start if it's there
        hex = hex.replace(/^#/, '');
    
        // Parse the r, g, b values
        let bigint = parseInt(hex, 16);
        let r = (bigint >> 16) & 255;
        let g = (bigint >> 8) & 255;
        let b = bigint & 255;
    
        return [r, g, b];
    }

    let rgb1 = hexToRgb(hex1);
    let rgb2 = hexToRgb(hex2);

    let r = Math.round(rgb1[0] + factor * (rgb2[0] - rgb1[0]));
    let g = Math.round(rgb1[1] + factor * (rgb2[1] - rgb1[1]));
    let b = Math.round(rgb1[2] + factor * (rgb2[2] - rgb1[2]));

    return rgbToHex(r, g, b);
}

function getColor(v) {
    // Colors
    const green = '#618B55';
    const yellow = '#B29f4C';
    const red = '#FF4444';

    let color;

    if (0.5 <= v && 1.0 >= v) {
        color = interpolateColor(yellow, red, (v * 2.0) - 1.0);
    } else if (0.5 > v && v >= 0.0) {
        color = interpolateColor(green, yellow, v * 2.0);
    } else {
        color = '#DDDDDD';
    }

    return color;
}

function style(feature) {
    // Calculate max value
    let state_max_count = Math.max(...Object.values(count_by_state["US"]));

    // Calculate color based on scaled value
    let color = getColor((count_by_state["US"][feature.properties.regionCode] || 0) / state_max_count);

    return {
        fillColor: color,
        weight: 2,
        opacity: 1,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.7
    };
}

function mapGlobalCountOverStates() {
    var geojson;
    var info = L.control();
    var legend = L.control({position: 'bottomright'});
    map = L.map('map').setView([37.8, -96], 4);

    var tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);

    // Highlighting

    function highlightFeature(e) {
        var layer = e.target;
    
        layer.setStyle({
            weight: 5,
            color: '#666',
            dashArray: '',
            fillOpacity: 0.7
        });
    
        layer.bringToFront();

        info.update(layer.feature.properties);
    }

    function resetHighlight(e) {
        geojson.resetStyle(e.target);

        info.update();
    }
    
    // Zoom on click

    function zoomToFeature(e) {
        map.fitBounds(e.target.getBounds());
    }

    function onEachFeature(feature, layer) {
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight,
            click: zoomToFeature
        });
    }

    // Info window

    info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'map_info'); // create a div with a class "info"
        this.update();
        return this._div;
    };

    info.update = function (props) {
        this._div.innerHTML = '<h4>Stats By State:</h4>' +  (props ?
            '<b>' + props.name + '</b><br />' + (count_by_state["US"][props.regionCode] || 0) + ' players'
            : 'Hover over a state');
    };

    info.addTo(map);

    // Legend

    let state_max_count = Math.max(...Object.values(count_by_state["US"]));

    function numberFormat(num) {
        return (Math.round(num * 100) / 100).toFixed(0);
    }

    legend.onAdd = function (map) {
    
        var div = L.DomUtil.create('div', 'map_info map_legend'),
            grades = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0],
            labels = [];
    
        // loop through our density intervals and generate a label with a colored square for each interval
        for (var i = 0; i < grades.length - 1; i++) {
            div.innerHTML +=
                '<i style="background:' + getColor(grades[i]) + '"></i> ' +
                numberFormat(grades[i] * state_max_count) + (numberFormat(grades[i + 1] * state_max_count) ? '&ndash;' + numberFormat(grades[i + 1] * state_max_count) + '<br>' : '');
        }
    
        return div;
    };
    
    legend.addTo(map);

    // GeoJSON shapes

    geojson = L.geoJson(statesData, {style: style, onEachFeature: onEachFeature}).addTo(map);
}

async function init() {
    // Setup
    //////////////////////////////////
    
    // Determine if in dev based on URL
    USING_DEV = false;  // location.origin.includes("dev.regexle.com") || /^https?:\/\/\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}.*$/.test(location.origin);

    // Attach event listener to date picker date
    document.querySelector('#date_picker_date').addEventListener('change', selectDay);
    document.querySelector('#date_picker_number').addEventListener('change', selectDay);

    // Attch event listener to size picker
    document.querySelector('#size_picker').addEventListener('change', selectSide);

    // Update maximum value of date picker
    document.querySelector("#date_picker_number").setAttribute('max', getTimes()[0]);

    //////////////////////////////////

    // Load the UUID from local storage
    loadUUID();

    // Get parameters from URL
    var searchParams = getSearchParams();

    // Check for URL uuid override
    if ("uuid" in searchParams) {
        UUID = searchParams["uuid"];
    }

    // Make request to back end
    data_by_uuid = await getDatabaseData("get_data_by_uuid", {"uuid": UUID});

    // Update on-screen text
    document.querySelector("#stats_raw_personal").innerHTML = JSON.stringify(data_by_uuid);

    // Make request to back end
    let data_all_time_by_side = await getDatabaseData("get_all_stats", {"side": 3});

    // Update on-screen text
    document.querySelector("#stats_raw_all_time").innerHTML = JSON.stringify(data_all_time_by_side);

    // Make request to back end
    let data_by_day_and_side = await getDatabaseData("get_daily_stats", {"day": getTimes()[0], "side": 3});

    // Update on-screen text
    document.querySelector("#stats_raw_selected_day").innerHTML = JSON.stringify(data_by_day_and_side);

    // Make request to back end
    times_by_day = await getDatabaseData("get_all_days_count", { "all_time": 0 , "side": 3});

    // Make request to back end
    count_by_state = await getDatabaseData("get_region_stats", { "country": "US", "all_time": 0, "side": 3 });

    //////////////////////////////////
    // Plot
    
    mapGlobalCountOverStates();
    plotPersonalTimesOverDays(3);
    plotGlobalCountOverDays(3);
}

document.addEventListener('DOMContentLoaded', init);
