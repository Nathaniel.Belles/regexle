# Regexle

Regexle is a daily hexagonal crossword puzzle game where you need to fill in the hexagons with character sequences to match the regular expressions ("RegEx") listed around the edges.

## Table of Contents

- [Regexle](#regexle)
  - [Table of Contents](#table-of-contents)
  - [Overview](#overview)
  - [Features](#features)
  - [Settings](#settings)
  - [Authors and Credits](#authors-and-credits)
  - [Feedback](#feedback)

## Overview

The goal of Regexle is to solve a daily hexagonal crossword puzzle by filling in the hexagons with alphanumeric character sequences that satisfy the given regex rules around the puzzle's edges.

## Features

- **Daily Puzzles:** A new puzzle every day with a unique challenge.
- **Interactive Board:** Clickable buttons for hints, settings, and information.
- **Settings Overlay:** Customize puzzle size, reset game data, enable high contrast mode, and more.
- **Info Overlay:** Detailed information and instructions on how to play the game.
- **Completion Overlay:** View your completion time, hint count, and share your results.
- **Hint Mode:** Click the lightbulb icon to enable hints. Correct cells are highlighted in green and incorrect cells in yellow.
- **Traversal:** Use Tab and Shift-Tab to navigate through the puzzle. Clicking a rule updates the traversal order to that rule's axis.

## Settings

- **Reset Board:** Resets the current game day and size.
- **Reset All Game Data:** Removes all game data, including saves and configurations.
- **Local Storage:** Indicates whether the game can store data on your device.
- **High Contrast Mode:** Adjusts coloring for better visibility.
- **Puzzle Size:** Change the length of characters on the hexagon side.
- **Puzzle Day:** Select a specific day for the puzzle.
- **Puzzle Credit:** Displays the puzzle author and time remaining for the next puzzle.
- **Authors and Credits:** Lists the authors and credits for the puzzle inspiration.
- **Feedback:** Link to report issues or provide feedback.

## Authors and Credits

- **Authors:**
  - [Nathaniel Belles](https://nathaniel.bell.es)
  - [Gable Brown](https://gablebrown.com/)
- **Inspired by:** 
  - [Jimbly's Regex Crossword](https://github.com/Jimbly/regex-crossword)
- **Debugging help:**
  - [Regex101](https://regex101.com)

## Feedback

If you encounter any issues or have feedback, please visit our [GitLab Issues Page](https://gitlab.com/Nathaniel.Belles/regexle/-/issues).