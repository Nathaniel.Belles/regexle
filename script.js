var board_data;
var selected_day;
var current_day;
var mid;
var diameter;
var user_data;
var highlighted_cell = [0, 0];

var USING_DEV = false;

const celebration_images = ["bill_nye.gif", "fairly_odd_parents.gif", "funny-celebrate-8.gif", "jazzy_skull.gif", "left_shark.gif", "raw.gif", "sliiiiide.gif", "bongocat-happy.gif", "frens.gif", "gooooooooooooal.gif", "le_office.gif", "oooooooosnap.gif", "shitty_bananas.gif", "snoop.gif"];

function loadPuzzle(data) {
    return JSON.parse(atob(data));
}

function shadeColor(color, percent) {
    var R = parseInt(color.substring(1, 3), 16);
    var G = parseInt(color.substring(3, 5), 16);
    var B = parseInt(color.substring(5, 7), 16);

    R = parseInt(R * (100 + percent) / 100);
    G = parseInt(G * (100 + percent) / 100);
    B = parseInt(B * (100 + percent) / 100);

    R = (R < 255) ? R : 255;
    G = (G < 255) ? G : 255;
    B = (B < 255) ? B : 255;

    R = Math.round(R);
    G = Math.round(G);
    B = Math.round(B);

    var RR = ((R.toString(16).length == 1) ? "0" + R.toString(16) : R.toString(16));
    var GG = ((G.toString(16).length == 1) ? "0" + G.toString(16) : G.toString(16));
    var BB = ((B.toString(16).length == 1) ? "0" + B.toString(16) : B.toString(16));

    return "#" + RR + GG + BB;
}

function toggleSettings(force = undefined) {
    // Get state of checkbox
    var is_settings_mode = document.querySelector('#settings_toggle_checkbox').checked;

    // Set the UUID
    var config = localLoadConfig();
    document.querySelectorAll('.uuid').forEach(el => el.textContent = config.uuid);

    // Update state 
    is_settings_mode = !is_settings_mode;

    // Check for override
    if (force !== undefined) {
        is_settings_mode = force;
    }

    // Update state of checkbox
    document.querySelector('#settings_toggle_checkbox').checked = is_settings_mode;

    // Settings enabled
    if (is_settings_mode) {
        // Formatting
        document.querySelector('#settings_overlay').style.display = 'flex';
        document.querySelector('#settings_toggle_image').src = './icons/close.svg';
        document.querySelector('#settings_toggle_image').style.width = '75%';
        document.querySelector('#settings_toggle_box').style.zIndex = '20';

        // Adjust theme color based on popup window background
        if (!document.querySelector('meta[name="theme-color"][custom]')) {
            // Create tag with custom property
            var meta = document.createElement('meta');
            meta.name = 'theme-color';
            meta.content = '#7f7f7f';
            meta.setAttribute('custom', '');
            document.head.appendChild(meta);
        }
        var color = document.querySelector('meta[name="theme-color"]:not([custom])')?.getAttribute("content");
        if (color) {
            document.querySelector('meta[name="theme-color"]:not([custom])').setAttribute("content", shadeColor(color, -50));
        }

        // Update only custom theme color
        document.querySelector('meta[name="theme-color"][custom]').setAttribute("content", "#7f7f7f");
    }
    // Settings disabled
    else {
        // Formatting
        document.querySelector('#settings_overlay').style.display = 'none';
        document.querySelector('#settings_toggle_image').src = './icons/gear.svg';
        document.querySelector('#settings_toggle_image').style.width = '100%';
        document.querySelector('#settings_toggle_box').style.zIndex = '';

        // Adjust theme color based on popup window background
        if (!document.querySelector('meta[name="theme-color"][custom]')) {
            // Create tag with custom property
            var meta = document.createElement('meta');
            meta.name = 'theme-color';
            meta.content = '#ffffff';
            meta.setAttribute('custom', '');
            document.head.appendChild(meta);
        }
        var color = document.querySelector('meta[name="theme-color"]:not([custom])')?.getAttribute("content");
        if (color) {
            document.querySelector('meta[name="theme-color"]:not([custom])').setAttribute("content", shadeColor(color, 100));
        }

        // Update only custom theme color
        document.querySelector('meta[name="theme-color"][custom]').setAttribute("content", "#ffffff");
    }
}

function toggleInfo(force = undefined) {
    // Get state of checkbox
    var is_info_mode = document.querySelector('#info_toggle_checkbox').checked;

    // Update state 
    is_info_mode = !is_info_mode;

    // Check for override
    if (force !== undefined) {
        is_info_mode = force;
    }
    
    // Update state of checkbox
    document.querySelector('#info_toggle_checkbox').checked = is_info_mode;

    // Store visited
    var config = localLoadConfig();
    if (force != true && !config["visited"]) {
        config["visited"] = true;
        localSave("config", config);
    }

    // Settings enabled
    if (is_info_mode) {
        // Formatting
        document.querySelector('#info_overlay').style.display = 'flex';
        document.querySelector('#info_toggle_image').src = './icons/close.svg';
        document.querySelector('#info_toggle_box').style.zIndex = '20';

        // Adjust theme color based on popup window background
        if (!document.querySelector('meta[name="theme-color"][custom]')) {
            // Create tag with custom property
            var meta = document.createElement('meta');
            meta.name = 'theme-color';
            meta.content = '#7f7f7f';
            meta.setAttribute('custom', '');
            document.head.appendChild(meta);
        }
        var color = document.querySelector('meta[name="theme-color"]:not([custom])')?.getAttribute("content");
        if (color) {
            document.querySelector('meta[name="theme-color"]:not([custom])').setAttribute("content", shadeColor(color, -50));
        }

        // Update only custom theme color
        document.querySelector('meta[name="theme-color"][custom]').setAttribute("content", "#7f7f7f");
    }
    // Settings disabled
    else {
        // Formatting
        document.querySelector('#info_overlay').style.display = 'none';
        document.querySelector('#info_toggle_image').src = './icons/info.svg';
        document.querySelector('#info_toggle_box').style.zIndex = '';

        // Adjust theme color based on popup window background
        if (!document.querySelector('meta[name="theme-color"][custom]')) {
            // Create tag with custom property
            var meta = document.createElement('meta');
            meta.name = 'theme-color';
            meta.content = '#ffffff';
            meta.setAttribute('custom', '');
            document.head.appendChild(meta);
        }
        var color = document.querySelector('meta[name="theme-color"]:not([custom])')?.getAttribute("content");
        if (color) {
            document.querySelector('meta[name="theme-color"]:not([custom])').setAttribute("content", shadeColor(color, 100));
        }
        // Update only custom theme color
        document.querySelector('meta[name="theme-color"][custom]').setAttribute("content", "#ffffff");
    }
}

function toggleCompletion(force = undefined) {
    var is_completion_mode = document.querySelector('#completion_toggle_checkbox').checked;

    // Update state 
    is_completion_mode = !is_completion_mode;

    // Check for override
    if (force !== undefined) {
        is_completion_mode = force;
    }
    // Update state of checkbox
    document.querySelector('#completion_toggle_checkbox').checked = is_completion_mode;
    // Completion enabled
    if (is_completion_mode) {
        // Formatting
        document.querySelector('#settings_toggle_box').style.zIndex = '20';
        document.querySelector('#completion_toggle_image').removeAttribute('hidden');
        document.querySelector('#settings_toggle_image').setAttribute('hidden', '');

        // Adjust theme color based on popup window background
        if (!document.querySelector('meta[name="theme-color"][custom]')) {
            // Create tag with custom property
            var meta = document.createElement('meta');
            meta.name = 'theme-color';
            meta.content = '#7f7f7f';
            meta.setAttribute('custom', '');
            document.head.appendChild(meta);
        }
        var color = document.querySelector('meta[name="theme-color"]:not([custom])')?.getAttribute("content");
        if (color) {
            document.querySelector('meta[name="theme-color"]:not([custom])').setAttribute("content", shadeColor(color, -50));
        }

        // Update share emoji string
        document.querySelector('#share_string').innerHTML = getEmojis(true);

        // Unselect all text entries
        document.querySelectorAll('.board_entry').forEach(el => el.blur());

        // Disable text input
        document.querySelectorAll('.board_entry').forEach(el => el.setAttribute("disabled", "true"));

        // Write permalink
        document.querySelector('#permalink').setAttribute("href", getPermalink());

        // Enable completion overlay and button
        document.getElementById("completion_overlay").style.display = "flex";

        // Update only custom theme color
        document.querySelector('meta[name="theme-color"][custom]').setAttribute("content", "#7f7f7f");
    }
    // Completion disabled
    else {
        // Formatting
        document.querySelector('#completion_toggle_image').setAttribute('hidden', '');
        document.querySelector('#settings_toggle_image').removeAttribute('hidden');
        document.querySelector('#settings_toggle_box').style.zIndex = '';

        // Adjust theme color based on popup window background
        if (!document.querySelector('meta[name="theme-color"][custom]')) {
            // Create tag with custom property
            var meta = document.createElement('meta');
            meta.name = 'theme-color';
            meta.content = '#ffffff';
            meta.setAttribute('custom', '');
            document.head.appendChild(meta);
        }
        var color = document.querySelector('meta[name="theme-color"]:not([custom])')?.getAttribute("content");
        if (color) {
            document.querySelector('meta[name="theme-color"]:not([custom])').setAttribute("content", shadeColor(color, 100));
        }

        // Re-enable text input
        document.querySelectorAll('.board_entry').forEach(el => el.removeAttribute("disabled"));

        // Hide completion overlay and button
        document.getElementById("completion_overlay").style.display = "none";

        // Update only custom theme color
        document.querySelector('meta[name="theme-color"][custom]').setAttribute("content", "#ffffff");
    }
}

function goToRandomPuzzle() {
    // Store updated random day
    var config = localLoadConfig();
    config["random_day"] -= 1;
    localSave("config", config);

    // Get current url to update
    var redirect_url = new URL(location.href);

    // Update day parameter
    redirect_url.searchParams.set("day", config["random_day"].toString());

    // Redirect to random day
    location.href = redirect_url.toString();
}

function getEmojis(html = false) {
    var board_emoji = "";

    for (var row = 0; row < diameter; row++) {
        board_emoji += html ? "" : "   ".repeat(Math.abs(row - mid));
        for (var column = 0; column < rowSize(row); column++) {
            switch (user_data.hint_board[row][column]) {
                // \u{2B1C} = ⬜ // \u{2B1B} = ⬛
                // No hints used
                case 0:
                case -1:
                    board_emoji += "\u{1F7E9}"; // 🟩
                    break;
                // Hint used
                default:
                    board_emoji += "\u{1F7E8}"; // 🟨
            }
        }
        board_emoji += html ? "</br>" : "\n";
    }

    return board_emoji;
}

function copyResults() {
    // Get emojis of board
    var emoji = getEmojis();

    // Create complete share string
    const share_string = "regexle.com #" + selected_day + ((mid + 1 != 3) ? " x" + (mid + 1) : "") + " \nTime: " + secondsToString(user_data["time"]) + "\nHints: " + user_data.hint_count + "\n\n" + emoji;

    console.log(share_string);

    // Attempt to share, otherwise copy
    if (navigator.share !== undefined) {
        // Trigger share sheet
        navigator.share({ text: share_string }); // Doesn't work on insecure browser
    } else {
        // Copy to clipboard
        navigator.clipboard.writeText(share_string);

        // Flash button text to indicate copied
        document.querySelector('#share_results').innerHTML = "Copied to Clipboard!";

        // Reset the value to the default after 5 seconds
        setTimeout(function () {
            document.querySelector('#share_results').innerHTML = "Share results!";
        }, 5000);
    }
}

function localLoadConfig() {
    const uuid = generateUUID();
    var ret = { "random_day": current_day < 0 ? current_day : 0, "colorblind": false, "visited": false, "uuid": uuid };
    try {
        ret = JSON.parse(localStorage["config"]);
    } catch (e) {
        // ignored, use default
    }
    return ret;
}

function localLoadStats() {
    var ret = {}; // "solve_count": {}, "time_sum": , "time_fastest": {}, "solves": {}
    try {
        ret = JSON.parse(localStorage["stats"]);
    } catch (e) {
        // ignored, use default
    }
    return ret;
}

function localLoad(key, deflt) {
    var ret = deflt;
    try {
        ret = JSON.parse(localStorage[key]);
    } catch (e) {
        // ignored, use default
    }
    return ret;
}

function localSave(key, value) {
    try {
        localStorage[key] = JSON.stringify(value);
    } catch (e) {
        // No localstorage
    }
}

function loadData() {
    user_data = localLoad('xword_data_' + selected_day + '_' + (mid + 1));

    // Initialize empty
    if (!user_data || !user_data.rows || !user_data.hint_board) {
        user_data = { rows: [], hint_count: 0, solved: false };

        user_data.hint_board = [];
        for (var row = 0; row < diameter; ++row) {
            user_data.rows.push("");
            user_data.hint_board.push([]);
            for (var column = 0; column < rowSize(row); ++column) {
                user_data.rows[row] += " ";
                user_data.hint_board[row].push(0);
            }
        }
    }
}

function saveUserDataToLocalStorage() {
    localSave('xword_data_' + selected_day + '_' + (mid + 1), user_data);
}

function rowSize(row) {
    var extra = row;
    if (extra > mid) {
        extra = diameter - 1 - row;
    }
    return mid + 1 + extra;
}

function boardsAreEqual(a, b) {
    // Compare two array-like boards elementwise for equality

    // Loop through rows
    for (var row = 0; row < a.length; ++row) {
        // Loop through columns
        for (var column = 0; column < a[row].length; ++column) {
            // Compare character in each board
            if (a[row][column] != b[row][column]) {
                // Return false for inequality
                return false;
            }
        }
    }

    // Return true for equality
    return true;
}

function getInputData() {
    // Get data from on-screen inputs and return as array

    var data = [];

    // Loop through rows
    for (var row = 0; row < diameter; ++row) {
        // Generate empty string for row
        data.push("");

        // Loop through columns
        for (var column = 0; column < rowSize(row); ++column) {
            // Get character at cell
            var character = document.querySelector('#id_' + row + '_' + column).value.toUpperCase();
            // Add character to current row
            data[row] += character ? character : " ";
        }
    }

    return data; // Like: ["AB", "CDE", "FG"]
}

function dataIsEmpty(data) {
    // Loop through rows
    for (var row = 0; row < diameter; ++row) {
        // Loop through columns
        for (var column = 0; column < rowSize(row); ++column) {
            // Check cell for empty space
            if (data[row][column] != ' ') {
                // Return not empty
                return false;
            }
        }
    }

    // Must have gotten all the way through
    return true;
}

function strReverse(str) {
    var ret = '';
    for (var ii = 0; ii < str.length; ++ii) {
        ret += str[str.length - ii - 1];
    }
    return ret;
}

function checkRules() {
    var isSolved = true;

    function check(str, axis, idx) {
        // Returns whether matched

        var rule = board_data[axis][idx];
        var regex = new RegExp('^' + rule + '$');
        var match = str.match(regex);
        if (match) {
            document.querySelector('#rule_' + axis + '_' + idx).classList.remove('nomatch');
            document.querySelector('#rule_' + axis + '_' + idx).classList.add('match');
            return true;
        } else {
            document.querySelector('#rule_' + axis + '_' + idx).classList.remove('match');
            document.querySelector('#rule_' + axis + '_' + idx).classList.add('nomatch');
            return false;
        }
    }

    for (var row = 0; row < diameter; ++row) {
        var str = '';
        for (var column = 0; column < rowSize(row); ++column) {
            str += user_data.rows[row][column];
        }

        if (!check(str, 'y', row)) {
            isSolved = false;
        }

        str = '';
        for (var column = 0; column < diameter; ++column) {
            var i = column;
            var j = row;
            if (column > mid) {
                j -= (column - mid);
            }
            if (user_data.rows[i][j] !== undefined) {
                str += user_data.rows[i][j];
            }
        }
        str = strReverse(str);

        if (!check(str, 'x', row)) {
            isSolved = false;
        }

        str = '';
        for (var column = 0; column < diameter; ++column) {
            var i = column;
            var j = row;
            if (column < mid) {
                j -= (mid - column);
            }
            if (user_data.rows[i][j] !== undefined) {
                str += user_data.rows[i][j];
            }
        }

        if (!check(str, 'z', row)) {
            isSolved = false;
        }
    }

    return isSolved;
}

function getRuleIndicesFromBoardIndices(row, column) {
    var ruleIndices = { 'x': -1, 'y': -1, 'z': -1 };

    ruleIndices['y'] = row;
    ruleIndices['x'] = row >= diameter - mid ? column + row - mid : column;
    ruleIndices['z'] = row >= mid ? column : column - row + mid;

    return ruleIndices;
}

function updateTabOrder(id) {
    // Clear tab index from all board entries
    document.querySelectorAll(".board_entry").forEach(el => el.setAttribute("tabindex", "-1"));

    // Get selected axis
    const [_, axis_str, index_str] = id.split("_");
    const index = parseInt(index_str);
    var desired_tabindex;
    var row_column_index;

    var focus_in_selected_rule = false;

    if (axis_str == "y") {
        focus_in_selected_rule = false;
        for (var row = 0; row < diameter; ++row) {
            for (var column = 0; column < rowSize(row); ++column) {
                row_column_index = row;
                desired_tabindex = column + 1;
                document.querySelector('#id_' + row + '_' + column).setAttribute("tabindex", row == index ? desired_tabindex : -1);

                if (row == highlighted_cell[0] && column == highlighted_cell[1] && row_column_index == index) {
                    focus_in_selected_rule = true;
                }
            }
        }

        // Only move focus to rule if not in rule already
        if (!focus_in_selected_rule) {
            // Select first entry along selected axis
            document.querySelector('#id_' + index + '_0').focus().select();
        } else {
            document.querySelector('#id_' + highlighted_cell[0] + '_' + highlighted_cell[1]).focus().select();
        }
    }

    // Handle x axis rules
    if (axis_str == "x") {
        focus_in_selected_rule = false;
        for (var row = 0; row < diameter; ++row) {
            for (var column = 0; column < rowSize(row); ++column) {
                row_column_index = row >= diameter - mid ? column + row - mid : column;
                if (column >= mid) {
                    desired_tabindex = diameter - 1 - row + 1;
                    document.querySelector('#id_' + row + '_' + column).setAttribute("tabindex", row_column_index == index ? desired_tabindex : -1);
                } else if (row >= diameter - 1 - mid) {
                    desired_tabindex = Math.min(column, diameter - 1 - row) + 1;
                    document.querySelector('#id_' + row + '_' + column).setAttribute("tabindex", row_column_index == index ? desired_tabindex : -1);
                } else {
                    desired_tabindex = column + diameter - 1 - row - (diameter - 1 - mid) + 1;
                    document.querySelector('#id_' + row + '_' + column).setAttribute("tabindex", row_column_index == index ? desired_tabindex : -1);
                }

                if (row == highlighted_cell[0] && column == highlighted_cell[1] && row_column_index == index) {
                    focus_in_selected_rule = true;
                }
            }
        }

        // Only move focus to rule if not in rule already
        if (!focus_in_selected_rule) {
            // Select first entry along selected axis
            if (index <= mid) {
                document.querySelector('#id_' + (mid + index) + '_0').focus().select();
            } else {
                document.querySelector('#id_' + (diameter - 1) + '_' + (index - mid)).focus().select();
            }
        } else {
            document.querySelector('#id_' + highlighted_cell[0] + '_' + highlighted_cell[1]).focus().select();
        }
    }

    // Handle z axis rules
    if (axis_str == "z") {
        focus_in_selected_rule = false;
        for (var row = 0; row < diameter; ++row) {
            for (var column = 0; column < rowSize(row); ++column) {
                row_column_index = row >= mid ? column : column + mid - row;
                if (column >= mid) {
                    desired_tabindex = row + 1;
                    document.querySelector('#id_' + row + '_' + column).setAttribute("tabindex", row_column_index == index ? desired_tabindex : -1);
                } else if (row < mid) {
                    desired_tabindex = Math.min(column, row) + 1;
                    document.querySelector('#id_' + row + '_' + column).setAttribute("tabindex", row_column_index == index ? desired_tabindex : -1);
                } else {
                    desired_tabindex = row + column - mid + 1;
                    document.querySelector('#id_' + row + '_' + column).setAttribute("tabindex", row_column_index == index ? desired_tabindex : -1);
                }

                if (row == highlighted_cell[0] && column == highlighted_cell[1] && row_column_index == index) {
                    focus_in_selected_rule = true;
                }
            }
        }

        // Only move focus to rule if not in rule already
        if (!focus_in_selected_rule) {
            // Select first entry along selected axis
            if (index <= mid) {
                document.querySelector('#id_' + (mid - index) + '_0').focus().select();
            } else {
                document.querySelector('#id_0_' + (index - mid)).focus().select();
            }
        } else {
            document.querySelector('#id_' + highlighted_cell[0] + '_' + highlighted_cell[1]).focus().select();
        }
    }
}

function processKeyEvents(elem, event) {
    // Returns bool hint about whether the board state changed

    if (event) {
        // Arrow keys
        if ([37, 38, 39, 40].includes(event.which)) {
            // Ensure only keyup event
            if (event.type !== 'keyup') {
                // Board data did not change
                return false;
            }

            var di = 0, dj = 0;
            if (event.which == 37) { // left arrow
                dj = -1;
            } else if (event.which == 38) { // up arrow
                di = -1;
            } else if (event.which == 39) { // right arrow
                dj = 1;
            } else if (event.which == 40) { // down arrow
                di = 1;
            }
            if (di != 0 || dj != 0) {
                var matches = elem.id.match(/\d+/g);
                var oi = parseInt(matches[0]);
                var oj = parseInt(matches[1]);

                var i = oi + di;
                var j = oj + dj;

                if (di == 1 && oi >= mid && oi < diameter - 1) {
                    j -= 1;
                }

                if (di == -1 && oi > mid) {
                    j += 1;
                }

                if (dj == -1 && oj == 0 && oi > mid) {
                    i -= 1;
                }

                if (dj == -1 && oj == 0 && oi < mid) {
                    i += 1;
                }

                if (dj == 1 && oj == rowSize(i) - 1 && oi > mid) {
                    i -= 1;
                }

                if (dj == 1 && oj == rowSize(i) - 1 && oi < mid) {
                    i += 1;
                }

                // Clamp i and j
                i = i > diameter - 1 ? diameter - 1 : (i < 0 ? 0 : i);
                j = j > rowSize(i) - 1 ? rowSize(i) - 1 : (j < 0 ? 0 : j);
                // log("output " + i + ", " + j)
                document.querySelector('#id_' + i + '_' + j).focus().select();

                // Board data did not change
                return false;
            }
        }

        // Backspace
        else if (8 == event.which) {
            // After deleting character
            if (event.type !== 'keydown') {
                // Board data did not change
                return false;
            }

            // Get current tabIndex
            const currentTabIndex = parseInt(elem.getAttribute('tabindex'));
            const currentValue = elem.value;

            // Currently empty cell
            if (currentValue == '' && currentTabIndex > 1) {
                // Focus last element
                document.querySelector('[tabindex="' + (currentTabIndex - 1) + '"]').focus();

                // Ignore next incoming backspace character
                event.preventDefault();

                // Board data did not change
                return false;
            }

            // Currently occupied cell
            if (currentValue != '') {
                // Clear current cell
                elem.value = '';

                // Ignore next incoming backspace character
                event.preventDefault();

                // Board data changed
                return true;
            }
        }

        // Tab and shift tab
        else if (9 == event.which) {
            // Before tab-ing
            if (event.type !== 'keydown') {
                // Board data did not change
                return false;
            }

            // Forward progression
            if (!event.shiftKey) {
                // console.log("Normal Tab");

                // Get current tabIndex
                const currentTabIndex = parseInt(elem.getAttribute('tabindex'));

                // Check if no rule selected for cycling
                if (currentTabIndex == 0) {
                    // Assume normal cycling

                    // Board data did not change
                    return false;
                }

                // Get maximum tabIndex
                var maxTabIndex = -1;
                document.querySelectorAll('.board_entry').forEach(el => maxTabIndex = Math.max(maxTabIndex, +el.getAttribute('tabindex')));

                // At end, wrap to beginning
                if (currentTabIndex == maxTabIndex) {
                    // Focus first element
                    document.querySelector('[tabindex="1"]').focus();

                    // Ignore next incoming tab character
                    event.preventDefault();
                }
            }
            // Reverse progression
            else {
                // console.log("Shifted Tab");

                // Get current tabIndex
                const currentTabIndex = parseInt(elem.getAttribute('tabindex'));

                // Check if no rule selected for cycling
                if (currentTabIndex == 0) {
                    // Assume normal cycling

                    // Board data did not change
                    return false;
                }

                // Get maximum tabIndex
                var maxTabIndex = -1;
                document.querySelectorAll('.board_entry').forEach(el => maxTabIndex = Math.max(maxTabIndex, +el.getAttribute('tabindex')));

                // At beginning, wrap to end
                if (currentTabIndex == 1) {
                    // Focus last element
                    document.querySelector('[tabindex="' + maxTabIndex + '"]').focus();

                    // Ignore next incoming tab character
                    event.preventDefault();
                }
            }

            // Board data did not change
            return false;
        }

        // Enter/Return key
        else if (13 == event.which) {
            // Before tab-ing
            if (event.type !== 'keydown') {
                // Board data did not change
                return false;
            }

            // Get current tabIndex
            const currentTabIndex = parseInt(elem.getAttribute('tabindex'));

            // Check if no rule selected for cycling
            if (currentTabIndex == 0) {
                // Assume normal cycling

                // Board data did not change
                return false;
            }

            // Get maximum tabIndex
            var maxTabIndex = -1;
            document.querySelectorAll('.board_entry').forEach(el => maxTabIndex = Math.max(maxTabIndex, +el.getAttribute('tabindex')));

            // At end, wrap to beginning
            if (currentTabIndex == maxTabIndex) {
                // Focus first element
                document.querySelector('[tabindex="1"]').focus();

                // Ignore next incoming return character
                event.preventDefault();
            }
            // Progress normally forward
            else {
                // Focus next element
                document.querySelector('[tabindex="' + (currentTabIndex + 1) + '"]').focus();

                // Ignore next incoming return character
                event.preventDefault();
            }

            // Board data did not change
            return false;
        }

        // Alphabetical characters
        else if (65 <= event.which && event.which <= 90) {
            // Before entering character
            if (event.type === 'keydown') {
                // Clear out value
                elem.value = '';
            }
            // After entering character
            else if (event.type === 'keyup') {
                // Get current tabIndex
                const currentTabIndex = parseInt(elem.getAttribute('tabindex'));

                // Get maximum tabIndex
                var maxTabIndex = -1;
                document.querySelectorAll('.board_entry').forEach(el => maxTabIndex = Math.max(maxTabIndex, +el.getAttribute('tabindex')));

                // Ensure valid tab index
                if (0 >= currentTabIndex) {
                    // Board data changed
                    return true;
                }

                // At end, wrap to beginning
                if (currentTabIndex == maxTabIndex) {
                    // Focus first element
                    document.querySelector('[tabindex="1"]').focus();

                    // Ignore next incoming tab character
                    event.preventDefault();
                }
                // Progress normally
                else {
                    // Focus next element
                    document.querySelector('[tabindex="' + (currentTabIndex + 1) + '"]').focus();
                }
            }

            // Board data changed
            return true;
        }

        // Other keys
        else {
            // Unlikely to have changed board data
            return false;
        }
    }

    // Board data might have changed
    return true;
}

function inputHasUpdated() {
    // Returns whether board has updated

    // Get all values from inputs
    const input_data = getInputData();

    // Compare against what is locally stored
    const has_updated = !boardsAreEqual(input_data, user_data.rows);

    // Return whether has changed
    return has_updated;
}

function onEvent(event, forceChange = false) {
    // Callback to run on any keyboard change event (keyup, keydown, change) or on timer

    // Process key events
    processKeyEvents(this, event);

    // Check if input has changed
    const changed = inputHasUpdated();
    // console.log("INPUT CHANGED? : " + changed);

    // Ensure inputs have changed
    if (!changed && !forceChange) {
        return;
    }

    // Copy input to user_data
    user_data.rows = getInputData();

    // Check if first time to write to storage
    if (!dataIsEmpty(getInputData())) {
        // Store the current time as start time
        if (!user_data["start"]) {
            user_data["start"] = getCurrentLocalTimeSeconds();
        }

        // Save user data to local storage
        saveUserDataToLocalStorage();
    }

    // Check rules
    var isSolved = checkRules();

    // Process solved
    if (isSolved) {
        // First time solving
        if (!user_data["finish"]) {
            // Store solved
            user_data["solved"] = true;

            // Store current time as completion time
            user_data["finish"] = getCurrentLocalTimeSeconds();

            // Calculate time to solve
            user_data["time"] = user_data["finish"] - user_data["start"];

            // Save user data to local storage
            saveUserDataToLocalStorage();

            // Load local statistics for solve first time
            loadInternalStatistics(true);
        }

        // Load local statistics for solve
        loadInternalStatistics();

        // Get statistics for solve from database
        loadExternalStatistics();

        // Set circles fill to green
        document.querySelectorAll('.board_entry').forEach(el => {
            el.style.backgroundColor = "rgb(97, 139, 85)";
            el.style.color = "rgb(255, 255, 255)";
        });

        // Update completion text
        document.querySelector('#puzzle_day').innerHTML = "Puzzle: #" + selected_day + ((mid + 1 != 3) ? " x" + (mid + 1) : "");
        document.querySelector('#hint_count').innerHTML = "Hints: " + user_data.hint_count;
        document.querySelector('#completion_time').innerHTML = "Time: " + secondsToString(user_data["time"]);

        // Show completion window
        toggleCompletion();
    }

    // Process not solved
    else {
        // Set circles fill to white
        document.querySelectorAll('.board_entry').forEach(el => el.style.backgroundColor = "white");

        // Set text to grey
        document.querySelectorAll('.board_entry').forEach(el => el.style.color = "#7f7f7f");
    }
}

function getCurrentLocalTimeSeconds() {
    // Get the current local time as seconds
    const now = new Date();
    const now_seconds = Math.floor(now / 1000);
    const timezone_seconds = now.getTimezoneOffset() * 60;
    const actual_seconds = now_seconds - timezone_seconds;

    return actual_seconds;
}

function onFocus() {
    this.select();
}

function onClickCell() {
    var elem = document.querySelector('#' + this.id.slice('wrap_'.length));
    elem.focus();
    elem.select();
}

function onFocusCell() {
    // Deselect all previous highlighted rules and cells
    document.querySelectorAll('.highlighted').forEach(el => el.classList.remove('highlighted'));
    document.querySelectorAll('.highlighted-cell').forEach(el => el.classList.remove('highlighted-cell'));

    // Get position of current cell
    var cellPositionMatch = this.id.match(/id_(\d+)_(\d+)/);
    var row = parseInt(cellPositionMatch[1], 10);
    var column = parseInt(cellPositionMatch[2], 10);
    var yAxisIndex = row;
    var xAxisIndex = yAxisIndex > mid ? column + row - mid : column;
    var zAxisIndex = yAxisIndex < mid ? column - row + mid : column;

    highlighted_cell = [row, column];

    // Set the relevant rules as highlighted
    document.querySelector('#rule_x_' + xAxisIndex).classList.add('highlighted');
    document.querySelector('#rule_y_' + yAxisIndex).classList.add('highlighted');
    document.querySelector('#rule_z_' + zAxisIndex).classList.add('highlighted');

    function highlightCell(row, column) {
        try {
            document.querySelector('#id_' + row + '_' + column).classList.add('highlighted-cell');
        }
        catch (error) {
            // console.log(`#id_${row}_${column} is not a valid cell`)
        }
    }

    for (var col = 0; col < rowSize(row); ++col) {
        highlightCell(row, col);
    }

    var tempXAxisIndex = xAxisIndex;
    for (var currentRow = 0; currentRow < diameter; ++currentRow) {
        if (currentRow > mid) {
            --tempXAxisIndex;
        }
        highlightCell(currentRow, tempXAxisIndex);
    }

    var tempZAxisIndex = zAxisIndex;
    for (var currentRow = diameter - 1; currentRow >= 0; --currentRow) {
        if (currentRow < mid) {
            --tempZAxisIndex;
        }
        highlightCell(currentRow, tempZAxisIndex);
    }
}


function reset() {
    // Update hint mode as disabled
    toggleHintMode(false);

    // Clear hint count
    document.querySelector("#live_hint_counter").innerHTML = "";

    // Clear current cells
    document.querySelectorAll('.board_entry').forEach(el => el.value = '');

    // Set hexagon fill to empty
    document.querySelectorAll('.hexagon_path').forEach(el => el.style.fill = "none");

    // Set circle fill to white
    document.querySelectorAll('.board_entry').forEach(el => el.style.backgroundColor = "white");

    // Clear storage for selected day
    localStorage.removeItem('xword_data_' + selected_day + '_' + (mid + 1));

    // Load data (recreate)
    loadData();

    // Check board updates
    onEvent(undefined, forceChange = true);
}

function reset_all() {
    // Reset the current board
    reset();

    // Remove all storage data
    localStorage.clear();
}

function ruleDisplay(axis, idx) {
    return board_data[axis][idx];
}

function getSearchParams() {
    var p = {};
    location.search.replace(
        /[?&]+([^=&]+)=([^&]*)/gi,
        function (s, k, v) { p[k] = v; }
    )
    return p;
}

function updateColorBlind() {
    // Get state of button
    var is_colorblind = document.querySelector('#colorblind_toggle').checked;

    var config = localLoadConfig();
    config["colorblind"] = is_colorblind;
    localSave("config", config);

    if (is_colorblind) {
        document.querySelectorAll(".page").forEach(el => {
            el.classList.add("colorblind");
            el.classList.remove("colornormal");
        });
        document.querySelectorAll(".mode_colornormal").forEach(el => el.style.display = "none");
        document.querySelectorAll(".mode_colorblind").forEach(el => el.style.display = "");
    } else {
        document.querySelectorAll(".page").forEach(el => {
            el.classList.remove("colorblind");
            el.classList.add("colornormal");
        });
        document.querySelectorAll(".mode_colornormal").forEach(el => el.style.display = "");
        document.querySelectorAll(".mode_colorblind").forEach(el => el.style.display = "none");
    }
}

function ruleMatches(axis, index) {
    var match = document.querySelector('#rule_' + axis + '_' + index).classList.contains('match');

    return match;
}

function toggleHintMode(force = undefined) {
    // Get state of checkbox
    var is_hint_mode = document.querySelector('#hint_toggle_checkbox').checked;

    // Update state 
    is_hint_mode = !is_hint_mode;

    // Check for override
    if (force !== undefined) {
        is_hint_mode = force;
    }

    // Update state of checkbox
    document.querySelector('#hint_toggle_checkbox').checked = is_hint_mode;

    // Increment hint count
    user_data.hint_count += (!user_data.solved) && (is_hint_mode) ? 1 : 0;

    // Update on screen hint count
    if (!user_data.solved) {
        document.querySelector('#live_hint_counter').innerHTML = user_data.hint_count;
    }

    if (is_hint_mode) {
        // Deselect all previous highlighted rules and cells
        document.querySelectorAll('.highlighted').forEach(el => el.classList.remove('highlighted'));
        document.querySelectorAll('.highlighted-cell').forEach(el => el.classList.remove('highlighted-cell'));

        // Set text to white
        document.querySelectorAll('.board_entry').forEach(el => el.style.color = "rgb(255, 255, 255)");

        // Disable text input
        document.querySelectorAll('.board_entry').forEach(el => el.setAttribute("disabled", "true"));

        // Override text opacity (iOS)
        document.querySelectorAll('.board_entry').forEach(el => el.style.opacity = "1.0");

        // Set background back to transparent
        document.querySelectorAll('.board_entry').forEach(el => el.style.backgroundColor = "transparent");

        // Set button background to grey
        document.querySelector('#hint_toggle_box').style.backgroundColor = "rgba(200, 200, 200, 1.0)";
    } else {
        // Set text to grey
        document.querySelectorAll('.board_entry').forEach(el => el.style.color = "#7f7f7f");

        // Re-enable text input
        document.querySelectorAll('.board_entry').forEach(el => el.removeAttribute("disabled"));

        // Set background back to white
        document.querySelectorAll('.board_entry').forEach(el => el.style.backgroundColor = "rgba(255, 255, 255, 1.0)");

        // Set background to clear again
        document.querySelector('#hint_toggle_box').style.background = "none";
    }

    // Get current solution
    const solution = board_data["solution"];

    // Get current inputs
    var current_input = getInputData();

    // Loop through all cells
    for (var row = 0; row < diameter; ++row) {
        for (var column = 0; column < rowSize(row); ++column) {
            var indices = getRuleIndicesFromBoardIndices(row, column);
            var indices_correct = { 'x': ruleMatches('x', indices['x']), 'y': ruleMatches('y', indices['y']), 'z': ruleMatches('z', indices['z']) };
            var selected_rules_correct = (indices_correct['x'] && indices_correct['y'] && indices_correct['z']);

            // Find empty cells
            if (current_input[row][column] == ' ') {
                if (is_hint_mode) {
                    // Empty
                    if ((!user_data.solved) && (user_data.hint_board[row][column] == 0)) {
                        // Set hexagon fill to grey
                        document.querySelector("#hex_path_" + row + "_" + column).style.fill = "rgba(230, 230, 230)";
                    }
                    // Set circle fill to grey
                    document.querySelector("#id_" + row + "_" + column).style.backgroundColor = "rgba(230, 230, 230)";
                } else {
                    // Empty
                    if (user_data.hint_board[row][column] == 0) {
                        // Set hexagon fill to transparent
                        document.querySelector("#hex_path_" + row + "_" + column).style.fill = "transparent";
                    }
                }
            }
            // Find incorrect cells 
            else if (current_input[row][column] != solution[row][column] && !selected_rules_correct) {
                if (is_hint_mode) {
                    // First time incorrect
                    if ((!user_data.solved) && ((user_data.hint_board[row][column] == 0) || (user_data.hint_board[row][column] == -1))) {
                        // Set hexagon fill to yellow
                        document.querySelector("#hex_path_" + row + "_" + column).style.fill = "rgb(178, 159, 76)";
                    }

                    // If previously correct, convert to number for counting
                    if (user_data.hint_board[row][column] == -1) {
                        user_data.hint_board[row][column] = 0;
                    }

                    // Increment hint if not solved
                    user_data.hint_board[row][column] += (!user_data.solved) ? 1 : 0;

                    // Set circle fill to yellow
                    document.querySelector("#id_" + row + "_" + column).style.backgroundColor = "rgb(178, 159, 76)";
                }
            }
            // Correct cells
            else {
                if (is_hint_mode) {
                    // First time correct
                    if ((!user_data.solved) && ((user_data.hint_board[row][column] == 0) || (user_data.hint_board[row][column] == -1))) {
                        // Store correct
                        user_data.hint_board[row][column] = -1;

                        // Set hexagon fill to green
                        document.querySelector("#hex_path_" + row + "_" + column).style.fill = "rgb(97, 139, 85)";
                    }

                    // Set circle fill to green
                    document.querySelector("#id_" + row + "_" + column).style.backgroundColor = "rgb(97, 139, 85)";
                }
            }
        }
    }

    // Save updated hint data and count
    saveUserDataToLocalStorage();

    // console.log(user_data.hint_board);
}

function log(string) {
    console.log(string);
};

function secondsToString(input) {
    if (input == 9 + 60) {
        return `${input}s nice!`;
    } else if (input == 60 * 7) {
        return `${input}s \u{1F343}`;
    } else if (input == 3 * 37 * 379) {
        return `${input}s`;
    } else {
        const hours = Math.floor(input / 60 / 60);
        const minutes = Math.floor((input - (hours * 60 * 60)) / 60);
        const seconds = Math.floor(input - (hours * 60 * 60) - (minutes * 60));

        const hours_str = (hours > 0) ? hours + "h" : "";
        const minutes_str = ((hours_str != "") ? " " : "") + ((hours > 0) || (minutes > 0) ? minutes + "m" : "");
        const seconds_str = ((minutes_str != "") ? " " : "") + seconds + "s";

        const str = hours_str + minutes_str + seconds_str;

        return str;
    }
}

function getTimes() {
    // Calculate current day and countdown to next day

    const SECONDS_PER_DAY = 86_400;
    const DAY_ZERO = 19_874;

    const now = new Date();
    const now_second = Math.floor(now / 1000);
    const timezone_second = now.getTimezoneOffset() * 60;
    const actual_second = now_second - timezone_second - (DAY_ZERO * SECONDS_PER_DAY);
    const actual_day = actual_second / SECONDS_PER_DAY;
    current_day = Math.floor(actual_day);
    const next_day = Math.ceil(actual_day);
    const next_second = next_day * SECONDS_PER_DAY;

    const countdown = next_second - actual_second;

    const countdown_str = secondsToString(countdown);

    return [current_day, countdown_str];
}

function updateCountdown() {
    document.querySelectorAll('.countdown').forEach(el => el.innerHTML = getTimes()[1]);
}

function cleanStorage() {
    // Remove saved boards from more than two days ago

    var keyCount = localStorage.length;
    var key, day;

    for (var i = 0; i < keyCount; i++) {
        key = localStorage.key(i);

        // Ensure valid key
        if (key.includes("xword_data_")) {
            // Get day from key
            day = parseInt(key.split('_')[2]);

            // Clear out storage from old days
            if (day < current_day - 1) {
                localStorage.removeItem(key);
            }
        }
    }
}

function getPermalink() {
    var permalink_url = new URL(location.origin);

    permalink_url.searchParams.append("day", selected_day);
    permalink_url.searchParams.append("side", side);

    return permalink_url.toString();
}

function copyPermalink() {
    if (navigator.share !== undefined) {
        navigator.share({ text: getPermalink() }); // Doesn't work on insecure browser
    } else {
        navigator.clipboard.writeText(getPermalink());
    }
}

function generateUUID() {
    const array = new Uint8Array(16);
    window.crypto.getRandomValues(array);

    array[6] = (array[6] & 0x0f) | 0x40;
    array[8] = (array[8] & 0x3f) | 0x80;

    const uuid = [...array].map((byte, i) => {
        if (i === 4 || i === 6 || i === 8 || i === 10) {
            return '-' + byte.toString(16).padStart(2, '0');
        }
        return byte.toString(16).padStart(2, '0');
    }).join('');

    return uuid;
}

function loadInternalStatistics(first = false) {
    /////////////////
    // Local stats

    var local_stats = localLoadStats();

    // First time completing
    if (first) {
        // Check first time creating stats document
        if (!local_stats[board_data["side"]]) {
            // Make default for that side
            local_stats[board_data["side"]] = { "solve_count": 0, "time_sum": 0, "time_fastest": Infinity, "solves": {} };
            local_stats[board_data["side"]]["streak"] = 0;

        }

        var today = parseInt(board_data["day"]);
        var yesterday = parseInt(board_data["day"]) - 1;
        var yesterday_str = (parseInt(board_data["day"]) - 1).toString();
        var yesterday_solved = local_stats[board_data["side"]]["solves"][(parseInt(board_data["day"]) - 1).toString()];

        console.log("today: " + today);
        console.log("yesterday: " + yesterday);
        console.log("yesterday_str: " + yesterday_str);
        console.log("yesterday_solved: " + yesterday_solved);

        // Increment streak
        if (current_day == selected_day) {
            if (local_stats[board_data["side"]]["streak"] >= 1 && yesterday_solved != undefined) {
                // Yesterday exists
                local_stats[board_data["side"]]["streak"] += 1;
            } else if (local_stats[board_data["side"]]["streak"] == 0) {
                // First day of streak
                local_stats[board_data["side"]]["streak"] += 1;
            } else {
                // Reset
                local_stats[board_data["side"]]["streak"] = 1;
            }
        }

        // Increment count
        local_stats[board_data["side"]]["solve_count"] += 1;

        // Update sum
        local_stats[board_data["side"]]["time_sum"] += user_data["time"];

        // Calculate fastest
        local_stats[board_data["side"]]["time_fastest"] = Math.min(user_data["time"], local_stats[board_data["side"]]["time_fastest"]);

        // Store solve
        local_stats[board_data["side"]]["solves"][board_data["day"]] = user_data["time"];

        // Store stats to local storage
        localSave("stats", local_stats);
    }

    // Update on-screen stats
    document.querySelector('#stats_solve_count').textContent = local_stats[board_data["side"]]["solve_count"];
    document.querySelector('#stats_solve_fastest').textContent = secondsToString(local_stats[board_data["side"]]["time_fastest"]);
    document.querySelectorAll('.stats_side').forEach(el => el.textContent = board_data["side"]);
    document.querySelectorAll('.stats_streak').forEach(el => el.textContent = local_stats[board_data["side"]]["streak"]);

    if (local_stats[board_data["side"]]["solve_count"] > 0 && local_stats[board_data["side"]]["time_sum"] > 0) {
        document.querySelectorAll(".stats_average").forEach(el => el.textContent = secondsToString(Math.round(local_stats[board_data["side"]]["time_sum"] / local_stats[board_data["side"]]["solve_count"])));
    } else if (local_stats[board_data["side"]]["solve_count"] > 0) {
        document.querySelectorAll(".stats_average").forEach(el => el.textContent = "0s");
    }
}

function applyOrdinalSuffix(num) {
    const suffixes = ["th", "st", "nd", "rd"];
    const value = num % 100;
    return num + (suffixes[(value - 20) % 10] || suffixes[value] || suffixes[0]);
}

function loadExternalStatistics() {
    /////////////////
    // External stats

    // Stats already loaded 
    if ("stats" in user_data && "percentile" in user_data["stats"] && "player_count" in user_data["stats"]) {
        // Update on-screen stats
        document.querySelector('#percentile').textContent = !/^-?\d+.?\d{0,2}$/.test(user_data["stats"]["percentile"]) ? (user_data["stats"]["percentile"] == "NaN" ? "0.00" : "---") : user_data["stats"]["percentile"];
        document.querySelector('#player_count').textContent = !/^-?\d+.?\d{0,2}$/.test(user_data["stats"]["player_count"]) ? "---" : applyOrdinalSuffix(user_data["stats"]["player_count"]);
        document.querySelectorAll('.stats_side').forEach(el => el.textContent = board_data["side"]);
    }
    // First time loading stats 
    else {
        const DEFAULT_DATABASE_URL = USING_DEV ? "https://dev-db.regexle.com" : "https://db.regexle.com";

        // Shape the data to send to the backend
        const to_send = {
            "day": board_data["day"],
            "size": board_data["side"],
            "backend_commit_hash": board_data["commit_hash"],
            "start_time": user_data["start"],
            "total_time": user_data["time"],
            "hint_count": user_data["hint_count"],
            "hint_board": JSON.stringify(user_data["hint_board"]),
            "uuid": localLoadConfig()["uuid"]
        };

        // Make request to backend
        var url = new URL(DEFAULT_DATABASE_URL);

        for (let key in to_send) {
            // console.log("Key: " + key + " value: " + to_send[key]);

            url.searchParams.append(key, to_send[key]);
        }

        log("database query: " + url);

        fetch(url).then(data => {
            return data.text();
        }).then(message => {
            try {
                // Get object from response from backend
                const database_data = JSON.parse(message);

                console.log("database response: " + JSON.stringify(database_data));

                // Update on-screen stats
                document.querySelector('#percentile').textContent = !/^-?\d+.?\d{0,2}$/.test(database_data["percentile"]) ? (database_data["percentile"] == "NaN" ? "0.00" : "---") : database_data["percentile"];
                document.querySelector('#player_count').textContent = !/^-?\d+.?\d{0,2}$/.test(database_data["player_count"]) ? "---" : applyOrdinalSuffix(database_data["player_count"]);
                document.querySelectorAll('.stats_side').forEach(el => el.textContent = board_data["side"]);

                // Write stats to local storage
                user_data["stats"] = {};
                for (var key in database_data) {
                    // Add variable
                    user_data["stats"][key] = database_data[key];
                }
                saveUserDataToLocalStorage();
            }
            catch (error) {
                log("error: " + error);
            }
        });
    }
}

function init() {
    const [current_day, countdown_str] = getTimes();

    // Parse URL parameters
    var url_params = getSearchParams();

    // Determine if in dev based on URL
    USING_DEV = location.origin.includes("dev.regexle.com") || /^https?:\/\/\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}.*$/.test(location.origin);

    const DEFAULT_BACKEND_URL = USING_DEV ? "https://dev-generator.regexle.com/api" : "https://generator.regexle.com/api"
    var backend = "backend" in url_params ? url_params["backend"] : DEFAULT_BACKEND_URL;

    var redirect_url = new URL(location.href);

    if ("day" in url_params) {
        // URL needs to change
        if (parseInt(url_params["day"]) >= current_day || !(/^-?\d+$/.test(url_params["day"]))) {
            // Redirect without day

            // Remove day parameter
            redirect_url.searchParams.delete("day");

            // Redirect
            window.location.replace(redirect_url.toString());
        } else {
            day = url_params["day"];
        }
    } else {
        // Default to current day
        day = current_day;
    }

    if ("side" in url_params) {
        // URL needs to change
        if (url_params["side"] == "3" || !(/^\d+$/.test(url_params["side"])) || parseInt(url_params["side"]) > 32 || parseInt(url_params["side"]) < 1) {
            // Redirect without side

            // Remove size parameter
            redirect_url.searchParams.delete("side");

            // Redirect
            window.location.replace(redirect_url.toString());
        } else {
            side = url_params["side"];
        }
    } else {
        // Default to side 3
        side = 3;
    }

    ///////////////////////////////

    var _a = setInterval(updateCountdown, 1000);

    selected_day = day;
    side = side;

    log("current_day: " + current_day);
    log("selected_day: " + selected_day);
    log("side: " + side);
    log("countdown: " + countdown_str);

    document.querySelector("#input_side").setAttribute('placeholder', side);
    document.querySelector("#input_day").setAttribute('placeholder', selected_day);

    document.querySelector("#input_side").setAttribute('placeholder', side);
    document.querySelector("#input_day").setAttribute('placeholder', selected_day);
    document.querySelector("#input_day").setAttribute('max', current_day);

    ///////////////////////////////
    // Make request to backend
    url = new URL(backend);
    url.searchParams.append("day", selected_day);
    url.searchParams.append("side", side);
    url.searchParams.append("include_solution", true);
    log("backend url: " + url);

    fetch(url).then(data => {
        return data.text();
    }).then(message => {
        try {
            // Get object from response from backend
            board_data = JSON.parse(message);
            // log("obj: " + JSON.stringify(obj));

            // log("backend response: " + JSON.stringify(board_data));

            // Load puzzle name
            document.querySelector('#puzzle_credit').innerHTML = "puzzle " + board_data['name'];

            // Test local storage
            if (window.localStorage) {
                document.querySelector('#nolocalstorage').style.display = 'none';
            } else {
                document.querySelector('#localstorage').style.display = 'none';
            }

            // Load high contrast setting
            document.querySelector("#colorblind_toggle").checked = localLoadConfig()["colorblind"];
            updateColorBlind();
            document.querySelector('#colorblind_toggle').addEventListener('change', updateColorBlind);

            // Get dimensions of board
            mid = (board_data.diameter - 1) / 2;
            diameter = board_data.diameter;

            // Attempt to load state from local storage
            loadData();

            // Load visited state to show info
            toggleInfo(!localLoadConfig()["visited"]);

            // Get index of image based on selected day
            const index = Math.abs(Math.floor((selected_day + (1000 * diameter)) % celebration_images.length));

            // Update image
            document.getElementById("completion_image").src = "/celebrate/" + celebration_images[index];

            // Used for hexagons (text input and shapes)
            function getTranslation(i, j, mid, already_centered) {
                const scale = 1; // Future scaling variable

                const offset = already_centered ? 0.0 : -0.5;
                const x = (j - mid + Math.abs(i - mid) / 2) + offset;
                const y = (i - mid) + offset;

                return [x, y];
            }

            // Generate html for board
            var lines = [];
            var row = [];
            var tabIndex = 1;

            for (var row = 0; row < diameter; ++row) {
                for (var column = 0; column < rowSize(row); ++column) {
                    var id = 'id_' + row + '_' + column;
                    var prev_value = (user_data.rows[row] && user_data.rows[row][column] || '');
                    if (prev_value === ' ') {
                        prev_value = '';
                        //prev_value = String.fromCharCode('A'.charCodeAt(0) + (row + column*3) % 26);
                    }

                    const font_size = 1.5;
                    const rule_spacing = 2.5;
                    const hex_size = 5.25;
                    const input_size = 2.25;

                    // Hexagon
                    var [translate_x, translate_y] = getTranslation(row, column, mid, true);
                    const scaling = mid > 2 ? 1.1 * 2 / mid + ((mid - 2) * 0.005) : 1.1;
                    lines.push('<div class="hexagon_center" style="transform: translate(' + translate_x * 2 * font_size * scaling + 'rem, ' + translate_y * 2 * font_size * 0.8660254 * scaling + 'rem);">'
                        + '<svg id="hex_' + row + "_" + column + '" class="hexagon" style="width: ' + (hex_size * scaling) + 'rem; height: ' + (hex_size * scaling) + 'rem:" viewBox="0 0 3000 3000" xmlns="http://www.w3.org/2000/svg" alt="Hexagon"><path class="hexagon_path" id="hex_path_' + row + "_" + column + '" fill="none" stroke="#7f7f7f" stroke-width="100" d="M 1500 500 L 633.974609 1000 L 633.974609 2000 L 1500 2500 L 2366.025391 2000 L 2366.025391 1000 Z"/></svg>'
                        + ((column == 0) ? '<div class="board_rule board_rule_y" id="rule_y_' + row + '" style="font-size: ' + (font_size * scaling) + 'rem; transform: translate(' + (-rule_spacing * scaling) + 'rem, -50%);" onclick="updateTabOrder(this.id)">' + ruleDisplay('y', row) + '</div>' : '')
                        + ((row == 0) || ((row != 0) && (row <= mid) && (column == rowSize(row) - 1)) ? '<div class="board_rule board_rule_x" id="rule_x_' + column + '" style="font-size: ' + (font_size * scaling) + 'rem; transform: translate(0%, -50%) rotate(-60deg) translate(' + (rule_spacing * scaling) + 'rem, 0%);" onclick="updateTabOrder(this.id)">' + ruleDisplay('x', column) + '</div>' : '')
                        + ((row == diameter - 1) || (((row >= mid) && row != diameter - 1) && (column == rowSize(row) - 1)) ? '<div class="board_rule board_rule_z" id="rule_z_' + column + '" style="font-size: ' + (font_size * scaling) + 'rem; transform: translate(0%, -50%) rotate(60deg) translate(' + (rule_spacing * scaling) + 'rem, 0%);" onclick="updateTabOrder(this.id)">' + ruleDisplay('z', column) + '</div>' : '')
                        + '<input class="board_entry" type="text" onkeydown="return /[a-zA-Z]/i.test(event.key)" maxlength="1" size="1" autocomplete="off" ' + 'value="' + prev_value + '" id="' + id + '" style="width: ' + (input_size * scaling) + 'rem; height: ' + (input_size * scaling) + 'rem; font-size: ' + (font_size * scaling) + 'rem;" enterkeyhint="next" tabindex="' + tabIndex + '"/>'
                        + '</div>');

                    // Increment tabIndex
                    tabIndex++;
                }
            }

            document.querySelector('.board_center').innerHTML = lines.join('\n');

            // Load hint counters
            if (user_data.hint_count > 0) {
                document.querySelector("#live_hint_counter").innerHTML = user_data.hint_count;
            }

            // Load hint visualization

            // Get current solution
            const solution = board_data["solution"];

            // Get current inputs
            getInputData();

            // Loop through all cells to update hexagon fill based on hint data
            for (var row = 0; row < diameter; ++row) {
                for (var column = 0; column < rowSize(row); ++column) {
                    // Hint used
                    if (user_data.hint_board[row][column] > 0) {
                        // Set hexagon fill to yellow
                        document.querySelector("#hex_path_" + row + "_" + column).style.fill = "rgb(178, 159, 76)";
                    }
                    // Was correct
                    else if (user_data.hint_board[row][column] == -1) {
                        // Set hexagon fill to green
                        document.querySelector("#hex_path_" + row + "_" + column).style.fill = "rgb(97, 139, 85)";
                    }
                }
            }

            var _b = setInterval(onEvent, 1000); // Process events every second in case events happened not through key events

            // Handle button and key presses
            document.querySelectorAll('.board_entry').forEach(el => el.addEventListener('change', onEvent));
            document.querySelectorAll('.board_entry').forEach(el => el.addEventListener('keydown', onEvent));
            document.querySelectorAll('.board_entry').forEach(el => el.addEventListener('keyup', onEvent));
            document.querySelectorAll('.board_entry').forEach(el => el.addEventListener('click', onFocus));
            document.querySelectorAll('.cell').forEach(el => el.addEventListener('click', onClickCell));
            document.querySelectorAll('.board_entry').forEach(el => el.addEventListener('focus', onFocusCell));
            document.querySelector('#reset').addEventListener('click', reset);
            document.querySelector('#reset_all').addEventListener('click', reset_all);

            onEvent(undefined, forceChange = true);

            // Add event listener for escape key
            document.addEventListener('keydown', function (event) {
                // Catch escape
                if (event.key === 'Escape') {
                    // Close all popups
                    toggleSettings(false);
                    toggleInfo(false);
                    toggleCompletion(false);
                }
            });

            cleanStorage();
        }
        catch (error) {
            log("error: " + error);
        }
    });

    ///////////////////////////////
}

document.addEventListener('DOMContentLoaded', init);